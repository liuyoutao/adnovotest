# Get all Request Urls
more ../AdEngineLog/localhost_access | grep GET | awk -F '"GET ' '{print $2}' | awk -F ' HTTP' '{print $1}' > ../AdEngineLog/allRequest.log

# Get uniq Adnovo ID
more ../AdEngineLog/allRequest.log | grep -v '&app_id=&' | awk -F '&app_id=' '{print $2}' |awk -F '&' '{print $1}' | sort|uniq >  ../AdEngineLog/AdnovoId_Online
