export ids=./Performance/AdnovoIDs
rm $ids
# Get enabled Adnovo IDs
../apache-jmeter-2.10/bin/jmeter.sh -n -t ./Performance/GetData.jmx
# Remove the header line.
more $ids | sed '1d' > temp
more temp > $ids

###########################################################################
# Below code check whether adnovo id is enough on verify environment.
export numberOnline=` more ../AdEngineLog/AdnovoId_Online |wc -l `;
export number=` more ./Performance/AdnovoIDs |wc -l `;

if [ $numberOnline -gt $number ]; then
export ratio=` expr $numberOnline / $number `;
	for((i=1;i<=$ratio;i++));
	do
	more ./Performance/AdnovoIDs >> ./Performance/AdnovoIDs;
	done
fi

more ./Performance/AdnovoIDs |head -n $numberOnline > temp
more temp >./Performance/AdnovoIDs

# Merge online adnovo id and test adnovo id.
pr -m -t -s,  ../AdEngineLog/AdnovoId_Online ./Performance/AdnovoIDs > temp
# Above code check whether adnovo id is enough on verify environment.
############################################################################
